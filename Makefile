build:
	echo "Run make install"
install:
	mkdir -p $(DESTDIR)/etc/pardus/
	mkdir -p $(DESTDIR)/usr/lib/pardus/device-blocker
	mkdir -p $(DESTDIR)/etc/udev/rules.d/
	mkdir -p $(DESTDIR)/usr/bin/
	mkdir -p $(DESTDIR)/etc/initramfs-tools/scripts/init-bottom/
	mkdir -p $(DESTDIR)/etc/initramfs-tools/hooks/
	install 00-device-blocker.rules $(DESTDIR)/etc/udev/rules.d/
	install *.py $(DESTDIR)/usr/lib/pardus/device-blocker
	install device-blocker $(DESTDIR)/usr/bin/
	install device-blocker.conf $(DESTDIR)/etc/pardus/
	install pci-remove.sh $(DESTDIR)/etc/initramfs-tools/scripts/init-bottom/
	install config-hook.sh $(DESTDIR)/etc/initramfs-tools/hooks/
