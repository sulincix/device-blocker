# Device blocker
usb device blocker

## Files
* /etc/pardus/device-blocker.conf : config file
* main.py : control and init device permissions
* device-blocker: command line interface
* usb.py : usb library

## Config
```
default: all devices default permission
host: usb host default permission
xxxxyyyy: specific device permission
```

You can use `device-blocker set <device id> <state>`

