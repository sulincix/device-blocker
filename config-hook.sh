#!/bin/sh -e

if [ "$1" = "prereqs" ]; then exit 0; fi
. /usr/share/initramfs-tools/hook-functions
mkdir -p $DESTDIR/etc/pardus/
cp /etc/pardus/device-blocker.conf $DESTDIR/etc/pardus/device-blocker.conf
