#!/usr/bin/python3
import os
import configparser
import usb
import sys
config = configparser.ConfigParser()
config.read('/etc/pardus/device-blocker.conf')

def get(section,variable,default):
    if section not in config:
        return default
    if variable not in config[section]:
        return default
    if config[section][variable].lower() == "true":
        return True
    if config[section][variable].lower() == "false":
        return False
    return config[section][variable]

# Authorize default from config
default = get("usb","default",True)

usbs = usb.list_usb()

if len(sys.argv) > 1:
    if sys.argv[1] == "list":
        usb.list_usb(True)
        exit(0)
    elif sys.argv[1] == "set":
        if len(sys.argv) > 3:
            config["usb"][sys.argv[2].replace(":","")] = sys.argv[3] 
            with open('/etc/pardus/device-blocker.conf', 'w') as configfile:
                 config.write(configfile)
        else:
            print("Usage: device-blocker set <devid> <true/false>")
            exit(1)
    elif sys.argv[1] == "pci":
        if len(sys.argv) > 3:
            key = sys.argv[3].replace(":","")
            if sys.argv[2] == "block":
                config["pci"][key] = "true"
            elif sys.argv[2] == "unblock":
                if key in config["pci"]:
                    config.remove_option("pci",key)
            elif sys.argv[2] == "list":
                os.system("lspci")
            with open('/etc/pardus/device-blocker.conf', 'w') as configfile:
                config.write(configfile)
            os.system("update-initramfs -u -k all")
            exit(0)
        if len(sys.argv) > 2:
            if sys.argv[2] == "rules":
                for key in config["pci"]:
                    key = key[:2]+":"+key[2:]
                    print("PCI blocked: {}".format(key))

        else:
            print("Usage: device-blocker pci <block/unblock/list> <devid>")
            exit(1)
    elif sys.argv[1] == "get":
        if len(sys.argv) > 2:
            print(get("usb",sys.argv[2].replace(":",""),default))
            exit(0)
        else:
            print("Usage: device-blocker get <devid>")
            exit(1)
    elif sys.argv[1] == "unset":
        if len(sys.argv) > 2:
            key = sys.argv[2]
            if key == "default":
                config["usb"][key] = "true"
            elif key == "host"or key == "removable":
                config["usb"][key] = "auto"
            else:
                config.remove_option("usb",key.replace(":",""))
            with open('/etc/pardus/device-blocker.conf', 'w') as configfile:
                 config.write(configfile)
        else:
            print("Usage: device-blocker unset <devid>")
            exit(1)
    elif sys.argv[1] == "rules":
        for key in config["usb"]:
            name=""
            for u in usbs:
                if key == u.id.replace(":",""):
                    name=u.name
            devid = key
            if key != "default" and key != "host" and key != "removable":
                devid = key[:4]+":"+key[4:]
            print("Found: ({})\t{}\t{}".format(get("usb",key, default), devid, name))
    else:
        print("Usage: device-blocker <set/get/unset/list> <options>")
        exit(1)


for u in usbs:
    state = get("usb",u.id.replace(":",""),default)
    if u.is_host():
        u.authorize_default(state)
        x = get("usb","host","auto")
        if x != "auto":
            state = x
    if u.is_removable():
        x = get("usb","removable","auto")
        if x != "auto":
            state = x
    u.authorize(state)
