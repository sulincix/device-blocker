#!/bin/sh
set +e
if echo $1 | grep prereqs > /dev/null ; then
    exit 0
fi
get_pci_rule(){
    e=0
    dev="$1"
    status=$(cat /etc/pardus/device-blocker.conf | tr -d " " | grep $dev | cut -f2 -d=)
    echo "$status" | grep -i true > /dev/null
    return $?
}

remove_pci(){
    pci=$(echo $1 | sed "s/^../&:/g")
    if [ -d /sys/bus/pci/devices/0000:${pci}/ ] ; then
        echo "PCI remove : ${pci}"
        echo 1 > /sys/bus/pci/devices/0000:${pci}/remove
    fi
}

e=0
cat /etc/pardus/device-blocker.conf | tr -s "\n" |tr -d " " | while read line ; do
    if [ $e -eq 0 ] ; then
        if echo "$line" | grep "[pci]" ; then
            e=1
            continue
        fi
    fi
    if [ $e -eq 1 ] ; then
        dev=$(echo $line | cut -f1 -d"=")
        if get_pci_rule $dev ; then
            remove_pci $dev
        fi
    fi
done
