import os, subprocess

def read(path):
    if not os.path.isfile(path):
        return ""
    with open(path,"r") as f:
        return f.read().strip()

def write(path,data):
    try:
        with open(path,"w") as f:
            f.write(data)
    except Exception as e:
        pass

class usb:
    def _find_name(self):
        for line in subprocess.getoutput("lsusb").split("\n"):
            if self.id in line:
                return " ".join(line.split(" ")[6:]).strip()
        return "Unknown"

    def is_host(self):
        return os.path.isfile(self.path+"/authorized_default")

    def is_removable(self):
        return read(self.path+"/removable") == "removable"

    def status(self):
        return read(self.path+"/authorized") == "1"

    def get_driver_type(self):
        for line in read(self.path+"uevent").split("\n"):
            if "DRIVER=" in line:
                return line.split("=")[1]
        return "usb"

    def __init__(self,path=""):
        self.path = path
        self.device = path.split("/")[-2]
        self.version = read(self.path+"version")
        self.id = (read(self.path+"/idVendor")+":"+read(self.path+"/idProduct")).strip()
        self.name = self._find_name()

    def authorize(self, status=True):
        if status:
            write(self.path+"/authorized","1")
        else:   
            write(self.path+"/authorized","0")

    def authorize_default(self, status=True):
        if not self.is_host():
            return
        if status:
            write(self.path+"/authorized_default","1")
        else:   
            write(self.path+"/authorized_default","0")

def list_usb(verbose=False):
    usbs = []
    ids = []
    for pci in os.listdir("/sys/bus/pci/devices/"):
        for directory in os.listdir("/sys/bus/pci/devices/{}".format(pci)):
            if directory.startswith("usb"):
                if os.path.exists("/sys/bus/pci/devices/{}/{}/idVendor".format(pci,directory)):
                        u=usb("/sys/bus/pci/devices/{}/{}/".format(pci,directory))
                        usbs.append(u)
                        if verbose:
                            print("Found:  ({})\t {}\t{}\t{}".format(u.status(), u.id, u.device, u.name))
                        
                for host in os.listdir("/sys/bus/pci/devices/{}/{}".format(pci,directory)):
                    if host[0].isnumeric() :
                        if os.path.exists("/sys/bus/pci/devices/{}/{}/{}/idVendor".format(pci,directory,host)):
                            u=usb("/sys/bus/pci/devices/{}/{}/{}/".format(pci,directory,host))
                            usbs.append(u)
                            if verbose:
                                print("Found:  ({})\t {}\t{}\t{}".format(u.status(), u.id, u.device, u.name))            
    return usbs

def authorize(devorid, status=True):
    for usb in list_usb():
        if usb.id == devorid or usb.device == devorid:
            usb.authorize(status)
            break

def authorize_byid(id,status=True):
    for usb in list_usb():
        if usb.id == id:
            usb.authorize(status)
            break

def authorize_bydev(dev,status=True):
    for usb in list_usb():
        if usb.device == dev:
            usb.authorize(status)
            break
